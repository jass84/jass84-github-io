---
permalink: /
title: "About me"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

* I am working with [Infosys](https://www.infosys.com/) as Senior Systems Engineer in the role of Technology Analyst for [Goldman Sachs's](https://www.goldmansachs.com/index.html) Investment Banking Business specializing in developing trade booking systems for [Securities](https://www.goldmansachs.com/what-we-do/global-markets/index.html).
* I received my Bachelor’s Degree from [Punjabi University](http://www.punjabiuniversity.ac.in) in 2017.


# Skills/Awards

* Programming: Java, Python, JavaScript, Shell Programming and Scripting, SQL, PL/ SQL,
HTML, CSS, Microsoft Excel
* [Coursera](https://www.coursera.org/user/5f986e4bd78cc280d4d6ee5e9e33005c) Certifications: 
  * Introduction to Data Science in Python by University of Michigan
  * Object Oriented Programming in Java by University of California San Diego
  * Java Programming: Solving Problems with Software by Duke University
* Virtual Internship: Software Engineering Virtual Experience by J.P Morgan Chase & Co.
* Linux Admin: DNS & DHCP, Cloud Management, NFS, ISCSI
* Received High Performer award at Infosys Ltd. for developing hotel booking module
* Ranked top 10 nationwide in Formula-3 car race championship by [Society of Automotive
Engineering SAE](https://www.saeindia.org/)

# Current Project
Architecting High&Low level System Design for helping onboard new line of Businesses Front-to-Back on Software Platform. Sample for learners [here](https://github.com/jass84/LoadBalancingExample)
